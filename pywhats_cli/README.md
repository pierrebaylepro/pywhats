# Client PyWhats 

## V1

### Objectifs de cas d'utilisation
- Envoyer message à un utilisateur
    - Transformer une saisie utilisateur en message
    - ajouter un message dans une conversation en locale 
    - transmettre message au serveur 
        - pas de vérification destinataire

- Recevoir message
    - Mettre à jour message lors de la connexion
    - Receptionner un message pendant le fonctionnement

## V2

- Authentifier un utilisateur 
- Identifier un destinataire 
    - demande de confirmation dest au serveur
