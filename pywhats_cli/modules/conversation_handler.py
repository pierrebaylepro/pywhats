import json
import logging
import os
from pathlib import Path
from modules.structs.conversation import Conversation

HISTORY_DIR = os.environ["HISTORY_DIR"]

class ConversationHandler:
    
    #################
    # Private methods

    def __init__(self, conv_name: str = None):
        self.conversation = None
        self.conv_path = ""

        self._load_conversation(conv_name)
        logging.debug(f"Loading {conv_name}")

    def _load_conversation(self, conv_name):
        if conv_name and self._conversation_file_exists(conv_name):
            self.conv_path = self._get_conversation_path(conv_name)
            self.conversation = Conversation(self.conv_path)
            self.conversation.load_conversation()

    def _conversation_file_exists(self, conv_name):
        return Path(self._get_conversation_path(conv_name)).exists()

    def _get_conversation_path(self, conv_name):
        return f"{HISTORY_DIR}/{conv_name}.json"

    def _initialize_new_conversation(self, users):
        self.conv_path = self._get_conversation_path(''.join(users).lower())
        self.conversation = Conversation(self.conv_path)
        self.conversation.set_users(users)
        self.conversation.status = "unread"
        self.conversation.save_conversation()
    
    
    #################
    # Public methods

    def create_conversation(self, users: list):
        users.sort()
        self._initialize_new_conversation(users)

    def get_conv_title(self):
        return self.conversation.get_title(self.conversation.get_users())
    
    def get_conv_users(self):
        return self.conversation.get_users()

    def get_conv_status(self):
        return self.conversation.get_status()
    
    def get_conv_checksum(self):
        return self.conversation.get_checksum()

    def get_conv_last_timestamp(self):
        return self.conversation.get_last_timestamp()
    
    def conv_exists(self):
        return self.conversation is not None

    def set_conv_status(self,status : str):
        self.conversation.set_status(status)
        self.conversation.save_conversation()

    def add_chat(self,chat : dict, exp : str):
        
        new_chat = {
        "timestamp": None,
        "user": None,
        "content": {
          "text": None,
          "file": None
        }
      }
        
        new_chat["timestamp"] = chat.get("timestamp")
        new_chat["user"] = exp
        new_chat["content"]["text"] = chat.get("text","")
        new_chat["content"]["file"] = chat.get("file","")
        
        self.conversation.chats.append(new_chat)
        self.conversation.save_conversation()
    
    def view_conversation(self):
        list_chats = self.conversation.get_chats()
        
        user_displayed = ""
        conversation_str = ""
        for chat in list_chats:
            if user_displayed != chat["user"]:
                user_displayed = chat["user"]
                conversation_str += f"\n{user_displayed.capitalize()}:\n"

            conversation_str += f"[{chat['timestamp'].split('T')[1]}] {chat['content']['text']}\n"    

        return conversation_str

