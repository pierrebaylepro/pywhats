import json

from modules.message_handler import MessageHandler
from modules.conversation_handler import ConversationHandler
from modules.misc import dict_parser

class ConversationSynchronizer:
    SYNC_BUFFER_SIZE = 8096
    RECEIVE_BUFFER_SIZE = 1024

    def __init__(self, client_socket, user):
        self.client_socket = client_socket
        self.user = user

    def synchronize_conversations(self):
        while True:
            sync_message = self.receive_sync_message()
            if sync_message is None:
                break

            sync_result = self.process_sync_message(sync_message)
            if isinstance(sync_result, dict):
                self.respond_to_sync(sync_message, sync_result)
                if not sync_result["up_to_date"]:
                    self.receive_and_process_chats(sync_result["timestamp"])
            else: 
                return 1
    def receive_sync_message(self):
        try:
            sync_message = self.client_socket.recv(self.SYNC_BUFFER_SIZE)
            return json.loads(sync_message)
        except Exception as e:
            print(f"Error receiving sync message: {e}")
            return None

    def process_sync_message(self, sync_message):
        message_handler = MessageHandler(self.user, sync_message)
        return message_handler.process_message()

    def respond_to_sync(self, sync_message, sync_result):
        sync_message["content"] = sync_result
        message_resp = json.dumps(sync_message)
        self.client_socket.send(message_resp.encode("utf-8"))

    def receive_and_process_chats(self,timestamp):
        new_chat = ""
        while "chat_sync_finished" not in new_chat:
            data = self.client_socket.recv(self.RECEIVE_BUFFER_SIZE).decode("utf-8")
            new_chat += data
        self.client_socket.send("received".encode("utf-8"))

        packets_to_add = dict_parser.parse_string_for_dict(new_chat)
        users = dict_parser.get_users_from_dict_list(packets_to_add, self.user.get_username())
        users.sort()
        conv_name = ''.join(users)
        conv_handler = ConversationHandler(conv_name) 

        if timestamp is None:
            conv_handler.create_conversation(users)
        
        for packet in packets_to_add:
            conv_handler.add_chat(packet["content"],packet["content"]["from"])
