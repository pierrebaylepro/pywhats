from pathlib import Path
import os
from modules.conversation_handler import ConversationHandler
from modules.misc.pywhats_exceptions import PyWhatsException

# This class should be used to display the text
# that the user will interact with

MENU_CONV = "   VOS CONVERSATIONS    \n"

class DisplayHandler():
    def __init__(self):
        self.current_conversation = None
        self.order_conv = []        # used to associate the number of a converstion in the converstion menu to a conversation name 
        pass

    def erase_screen(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def get_current_window(self):
        return self.current_conversation
    
    def show_list_convs(self):
        self.order_conv
        
        dir_conv = Path(os.environ["HISTORY_DIR"])
        list_conv = []
        menu_to_return = str(MENU_CONV)

        for item in dir_conv.iterdir():
            if item.is_file() and item.suffix.lower() == ".json":
                conv_handler = ConversationHandler(item.name.replace(".json",""))
                self.order_conv.append(item.name.replace(".json",""))
                list_conv.append((conv_handler.get_conv_title(), conv_handler.get_conv_status() == "unread"))

        
        to_display = self._format_conv_status_(list_conv)
        if to_display == "":
            menu_to_return += "No conversation yet\n"
        else:
            menu_to_return += to_display
        
        return menu_to_return
    
    def show_conversation(self,num_conv : str, username : str):
        conv_name = self.order_conv[num_conv - 1]
        conv_handler = ConversationHandler(conv_name)
        
        if not conv_handler.conv_exists() :
            raise PyWhatsException("WRONG_CONV_NAME")
        
        self.set_current_conversation(conv_name)
        conv_handler.set_conv_status("read")
        return conv_handler.view_conversation()

    def _format_conv_status_(self,list_conv: list[tuple]):
        output = ""
        i = 1
        for conv in list_conv:
            current_conv = conv[0]
            status = conv[1]
            new_line = f"{i}. {current_conv.ljust(30)}{'NEW' if status else ''}"
            output += new_line + "\n"

            i += 1
            
        return output

    def set_current_conversation(self,conv_name):
        self.current_conversation = conv_name

    def get_current_conversation(self):
        return self.current_conversation
    
    def get_num_conv_from_name(self,conv_name):
        return self.order_conv.index(conv_name) + 1
    
    def get_number_convs(self):
        return len(self.order_conv)