import logging 
import os
from pathlib import Path 
from modules.structs.user import User 
from modules.conversation_handler import ConversationHandler
from modules.structs.message import Message
# {
#     "packet_type" : "chat",
#     "content": {
#         "from" : "nom_utilisateur",
#         "text" : "tata toto titi",
#         "file" : "file_id_on_server",
#         "timestamp" : "",
#         "isGroupChat" : ["true ou false","si [0] == true : identifiant groupe"]
#     }, 
#     "identifier" : "",
#     "token" : ""
# }

BASIC_MESSAGE = {
    "packet_type" : "",
    "content": None, 
    "identifier" : "",
    "token" : ""
}

class MessageHandler:

    def __init__(self, user : User ,message: dict = None):
        self.message = message
        self.connectedUser = user
        # if message is not None:
        #     self.process_message(message)
   
    def process_message(self):
        try: 
            mess = Message(self.message)
        except KeyError as e:
            print(f"{e=}") # Change this with proper PyWhatsException

        if self.message["packet_type"] == "chat":
            logging.info(f"Received this message : {self.message['content']}")
            self.process_received_chat_message(self.message["content"])
        if self.message["packet_type"] == "sync":
            return self.process_sync_message(self.message["content"])

    def process_received_chat_message(self, message : dict):
        expeditor = message["from"]
        isGroupChat = message.get("isGroupChat",None)

        if isGroupChat :
            pass
        else :
            current_username = self.connectedUser.username
            name_conv = [current_username.lower(),expeditor.lower()]
            name_conv.sort()
            name_conv = "".join(name_conv)
            
            # TODO ?
            # Add some sort of verification of the content's structure
            # END TODO

            ch = ConversationHandler(name_conv)
            print(f"New message added in {name_conv} : {message}")
            ch.add_chat(message,expeditor)
            ch.set_conv_status("unread")
            return True
        
        # if not conversation_exists:
            

    def process_sync_message(self, message : dict):
        conv_exists = None
        last_timestamp = None
        is_up_to_date = None
        
        finished_sync = message.get("status",None)
        if finished_sync is not None:
            return True

        conv_name = message.get("conv_name",None)
        checksum = message.get("checksum", None)

        if conv_name is None or checksum is None:
            # handle error
            return 0 

        conv = ConversationHandler(conv_name)
        conv_exists = conv.conv_exists()

        if conv_exists:
            is_up_to_date = checksum == conv.get_conv_checksum()

            if not is_up_to_date:
                last_timestamp = conv.get_conv_last_timestamp()
        
        message.update({"up_to_date":is_up_to_date, "timestamp" : last_timestamp}) 
        
        return message
    
    def get_chat_message(self, chat :dict,  groupChat : list = None):
        chat_message = {
            "packet_type" : "chat",
            "content": {
                "to" : "",
                "file" : "",
                "text" : "",
                "timestamp" : "",
                "isGroupChat" : ""
            }, 
            "identifier" : "",
            "token" : ""
        }

        # Retrieving the receivers by removing the sender from users' list 
        groupChat.remove(chat["user"])
        dest = groupChat
        # Can be refactored once tested
        chat_message["content"]["to"] = dest
        chat_message["content"]["file"] = chat["file"]
        chat_message["content"]["text"] = chat["text"]
        chat_message["content"]["timestamp"] = chat["timestamp"]
        
        if len(groupChat) > 2 :
            chat_message["content"]["isGroupChat"] = groupChat

        return chat_message