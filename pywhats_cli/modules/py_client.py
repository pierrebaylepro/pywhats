import socket
import threading
import json
import sys

from datetime import datetime

from modules.message_handler import MessageHandler
from modules.conversation_handler import ConversationHandler
from modules.conversation_synchronizer import ConversationSynchronizer
from modules.display_handler import DisplayHandler
from modules.structs.user import User
from modules.misc import dict_parser

class PyWhatsClient:
    def __init__(self, ip_address, port, display_handler : DisplayHandler ):
        self.ip_address = ip_address
        self.port = port
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        self.user = None
        self.token = None

        self.display_handler = display_handler

        #commandlist
        self.commands = ["!connect", "!quit", "!view","!home","!changeNickname", "!cn", "!help", "!upload", "!up", "!showFiles", "!download", "!dl", "!changeBio", "!cb", "!displayBio", "!db", "!showMembers", "!sm"]
        
        #help to be displayed by the user when needed 
        self.help = """
                        !connect <pseudonyme> - Se connecter avec le pseudonyme donné en argument.\n
                        !quit - Se déconnecter.\n
                        !home - afficher les conversations
                        !changeNickname <pseudonyme> ou !cn <pseudonyme> - Modifier son pseudonyme par celui spécifié en argument.\n
                        !help - Consulter la liste des commandes.\n
                        !upload <chemin> ou !up <chemin> - Envoyer le fichier dont le chemin absolu est spécifié en argument.\n
                        !showFiles ou !sf - Consulter la liste des fichiers disponibles au téléchargement.\n
                        !download <nom_fichier> ou !dl <nom_fichier> - Télécharger un fichier dont le nom est passé en argument.\n
                        !changeBio ou !cb- Modifier sa bio.\n
                        !displayBio <pseudonyme>ou !db <pseudonyme> - Afficher la bio de l’utilisateur passé en argument.
                        !showMembersou ou !sm- Afficher la liste des participants.\n
                        """

    # Attempts to connect the socket and user to the server
    def connect_to_server(self) :
        self.client_socket.connect((self.ip_address, self.port))
        
    
    #close the connection
    def close_connection(self) :
        self.client_socket.close()
        

    # Move this to the message handler
    def auth_user(self,creds : tuple):
        auth_message = {"packet_type" : "connect",
                        "content": {
                            "username" : creds[0],
                            "password": creds[1] 
                        } , 
                        "identifier" : "test",
                        "token" : "token"
        }
        self.client_socket.send(json.dumps(auth_message).encode("utf-8"))
        resp = self.client_socket.recv(8096)

        # Check if the response contain an authentication token, else read and return the error
        dict_resp = json.loads(resp)

        if list(dict_resp["content"].keys())[0] == "token":
            self.token =dict_resp["content"]["token"]
            self.user = User(creds[0])
            return True, None
        else :
            return False, dict_resp["content"]["error"]
        
    #generate a dictionnary
    def generate_dic(self, user_input):
        is_a_command = False
        dic = {}

        #check if the input is a command
        for pattern in self.commands:
            if (str.lower(user_input)).startswith(str.lower(pattern) + " ") or str.lower(user_input) == str.lower(pattern):
                is_a_command = True
                dic["command"] = pattern
                user_input = user_input[len(dic["command"]):].lstrip()
                break
        
        if not is_a_command:
            dic["command"] = "chat"

        dic["args"] = user_input
        dic["content"]=dict()

        return dic

    #check the number of arguments and build the right dictionnary depending on the commandline
    #return message error or if the command is to be handled localy or remotely
    def check_command(self, dic):

        dic["command"]

        #alias handling
        if (str.lower(dic["command"]) == "!cn"):
            dic["command"] = "!changeNickname"

        if (str.lower(dic["command"]) == "!up"):
            dic["command"] = "!upload"

        if (str.lower(dic["command"]) == "!sf"):
             dic["command"] = "!showFiles"

        if (str.lower(dic["command"]) == "!dl"):
             dic["command"] = "!download"

        if (str.lower(dic["command"]) == "!cb"):
             dic["command"] = "!changeBio"

        if (str.lower(dic["command"]) == "!db"):
            dic["command"] = "!displayBio"

        if (str.lower(dic["command"]) == "!sm"):
             dic["command"] = "!showMembers"

        res = ""

        #switch case per command
        match str.lower(dic["command"]):
            # case "!connect":
            #     if (len(dic["args"].split()) != 2) :
            #         res = "La commmande de connexion prend 2 arguments : !connect <pseudo> <mdp>"
            #     else:
            #         dic["content"]["username"]=dic["args"].split()[0]
            #         dic["content"]["password"]=dic["args"].split()[1]
            #         res = "remote"
            
            case "!quit":
                res = "local"
            
            case "!changenickname":
                if (len(dic["args"].split()) != 1) :
                    res = "La commmande de changement de pseudo prend 1 argument : !changeNickname <pseudo>"                    
                
            case "!upload":
                if (len(dic["args"].split()) != 1) :
                    res = "La commmande !upload prend 2 arguments : !upload <chemin>"
                       
            case "!download":
                if (len(dic["args"].split()) != 1) :
                    res = "La commmande !download prend 1 argument : !download <nom_fichier>"
                
            case "!displaybio":
                if (len(dic["args"].split()) != 1) :
                    res = "La commmande !displayBio prend 1 argument : !displayBio <pseudonyme>"
                
            
            # case "!changeBio":
            #     #TODO dialog with the user to determine what to send to the server
            #     res = "remote"
            
            case "!showMembers":
                res = "remote"

        command = (dic["command"].lower()) 
        if res == "" and command in ["!showmembers", "!changebio", "!showfiles", "!displaybio", "!download","!upload","!changenickname"] :
            res = "remote"
        elif res == "" :
            res = "local"

        return res

    #generate a json from a dictionnary
    def generate_json(self, dic):
          compliant_dic = {"packet_type": dic['command'].lstrip("!") , "content": dic["content"], "identifier": "", "token": ""}
          return json.dumps(compliant_dic, indent=4)

    #execute local commands
    def execute_local_command(self, dic):
        res = ""
        match str.lower(dic["command"]):
           
            case "!quit":
                self.close_connection()
                res = "quit"
            case "!help":
                res = "help"
            case "!view":
                res = "view"
            case "!home":
                res = "home"
            case "chat":
                res = "chat"
        return res


    #Handles client's input
    def input_handler(self):
        while True:
            #Get the user's input
            user_input = input("::")
            
            #parse the input
            dic = self.generate_dic(user_input)
            checked_result = self.check_command(dic)

            #handles local commands
            if (checked_result == "local"):
                res = self.execute_local_command(dic)
                if res == "quit":
                    print("Au revoir !")
                if res == "home":
                    self.display_handler.erase_screen()
                    self.display_handler.set_current_conversation(None)
                    home_view = self.display_handler.show_list_convs()
                    print(home_view)
                if res == "view":
                    if dic["args"] == 0:
                        print("You need to provide a conversation number")
                    elif not str(dic["args"]).isdigit() or int(dic["args"]) < 1 or int(dic["args"]) > self.display_handler.get_number_convs():
                        print("You didn't provide a valid conversation number")
                    else:
                        conversation = self.display_handler.show_conversation(int(dic["args"]),self.user.get_username())
                        self.display_handler.erase_screen()
                        print(conversation)
                if res == "help" :
                    self.display_handler.erase_screen()
                    print(self.help)
                if res == "chat":
                    current_conv = self.display_handler.get_current_conversation()
                    if current_conv is not None:
                        self.send_new_text_chat(dic["args"])
                        # self.display_handler.erase_screen()
                        conv_num = self.display_handler.get_num_conv_from_name(current_conv)
                        print(conv_num)
                        updated_view = self.display_handler.show_conversation(conv_num,self.user.get_username())
                        print(updated_view)
                    else:
                        print("Vous devez vous positionner dans une conversation avec !view pour envoyer un chat ou saisir une commande valable")

            #handles commands send to server
            elif (checked_result == "remote"):
                data_to_send = self.generate_json(dic)
                #Send the message via socket
                self.client_socket.send(bytes(data_to_send, "utf-8"))

            #errors
            else :
                print(checked_result)
        
    #manage recieved messages from the server
    def receive_message(self):
        while True:
            try:
                message = self.client_socket.recv(8096).decode("utf-8")
                message_dict = json.loads(message)
                MessageHandler(self.user, message_dict).process_message()

            except Exception as e:
                print(f"{e=}")

    def synchronize_conversations(self):
        synchronizer = ConversationSynchronizer(self.client_socket,self.user)
        synchronizer.synchronize_conversations()
        
    def send_new_text_chat(self,chat_text : str):
        
        conv_handler = ConversationHandler(self.display_handler.get_current_conversation()) 
        formatted_new_chat = self.get_new_chat(chat_text)
        conv_handler.add_chat(formatted_new_chat,self.user.get_username())
        
        chat_message = MessageHandler(self.user).get_chat_message(formatted_new_chat,conv_handler.get_conv_users())
        formatted_chat_message = json.dumps(chat_message)
        print(f"Sending chat message : {formatted_chat_message}")
        self.client_socket.send(formatted_chat_message.encode("utf-8"))
        

    def get_new_chat(self, text : str = None, file : str = None):
        new_chat = {
            "timestamp": None,
            "user": None,
            "text": None,
            "file": None
        }

        new_chat["timestamp"] = datetime.isoformat(datetime.now(),sep="T",timespec="seconds")
        new_chat["user"] = self.user.get_username()
        new_chat["text"] = text
        new_chat["file"] = file
        
        return new_chat

