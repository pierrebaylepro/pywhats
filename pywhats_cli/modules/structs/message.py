# {
#     "packet_type" : "chat",
#     "content": {
#         {"from" : "nom_utilisateur"},
#         {"text" : "tata toto titi"},
#         {"file" : "file_id_on_server"},
#         {"timestamp" : ""},
#         "isGroupChat" : ["true ou false","identifiant groupe"]
        

#     }, 
#     "identifier" : "",
#     "token" : ""
# }

BASIC_MESSAGE = {
    "packet_type" : "",
    "content": None, 
    "identifier" : "",
    "token" : ""
}

class Message():
    
    def __init__(self,message):
        for key in BASIC_MESSAGE.keys():
            if str(key) not in message.keys():
                raise KeyError
            
