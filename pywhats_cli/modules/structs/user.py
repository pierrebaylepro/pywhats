import json
import logging
from modules.misc.pywhats_exceptions import PyWhatsException  

class User:

    def __init__(self, username: str, password: str = None):
        self.username = username
        self.userInfos = None

    def get_username(self):
        return self.username

    