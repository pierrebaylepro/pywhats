import os
os.environ["HISTORY_DIR"] = "/media/eff/4A4241B74241A891/Users/Admin/Desktop/M1/projetPython/pywhats/pywhats_cli/history"

from modules.structs.conversation import Conversation
from modules.conversation_handler import ConversationHandler
from pathlib import Path 
import json
import unittest

class TestAuth(unittest.TestCase):
    def setUp(self) :
        self.new_conversation = None
        self.conv_handler = None
        self.conv_name = "alicebob"
        self.path_conv = Path(f"{os.environ['HISTORY_DIR']}/{self.conv_name}.json").resolve()

    def testInitConversation(self):
        
        self.new_conversation = Conversation(self.path_conv)
        self.new_conversation.load_conversation()

        self.assertEqual(self.new_conversation.title.lower(),"alice et bob")
        self.assertEqual(self.new_conversation.status.lower(),"unread")
        self.assertEqual(len(self.new_conversation.messages),5)

    def testInitConversationHandler(self):
        self.conv_handler = ConversationHandler(self.conv_name)
        self.assertTrue(isinstance(self.conv_handler.conversation,Conversation))

    def testConversationHandlerGetTitle(self):
        self.conv_handler = ConversationHandler(self.conv_name)
        self.assertEqual(self.conv_handler.get_conv_title().lower(),"alice et bob")

    def testAddingNewMessages(self):
        MESSAGE = {"from" : "albert", "text" : "Hello World", "file" : "", "timestamp" : "1706024497.486117", "isGroupChat" : ["False"]}
      
        if not Path(f"{os.environ['HISTORY_DIR']}/albertpatrick.json").exists():
            print("Creating the albertpatrick.json for the test")
            self.conv_handler = ConversationHandler("albertpatrick")        
            self.conv_handler.create_conversation(["Albert","Patrick"])
            self.conv_handler = None

        self.conv_handler = ConversationHandler("albertpatrick")
        self.conv_handler.add_chat(MESSAGE)
      
        with open(Path(f"{os.environ['HISTORY_DIR']}/albertpatrick.json"),"r") as f:
            test_dict = json.load(f)
            self.assertEqual(MESSAGE["timestamp"],test_dict["messages"][0]["timestamp"])

    def testCreatingNewConversation(self):
        
        user1 = "Patrick"
        user2 = "Albert"
        title_conv = [user1,user2]
        title_conv.sort()
        title_conv = "".join(title_conv)
        title_conv = title_conv.lower()
        
        test_json_file = f"{os.environ['HISTORY_DIR']}/{title_conv}.json"
        if Path(test_json_file).exists():
            os.remove(test_json_file)

        self.conv_handler = ConversationHandler(title_conv)        
        self.conv_handler.create_conversation([user1,user2])
        
        self.assertTrue(Path(test_json_file).exists())

        
if __name__ == "__main__":
    if os.environ["HISTORY_DIR"] == "":
        raise Exception("You need to provide the path to the history/ dir for testing purposes")
    unittest.main()