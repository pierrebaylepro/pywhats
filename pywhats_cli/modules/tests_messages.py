import os
os.environ["HISTORY_DIR"] = "/media/eff/4A4241B74241A891/Users/Admin/Desktop/M1/projetPython/pywhats/pywhats_cli/history"

from modules.structs.message import Message
from modules.message_handler import MessageHandler
from pywhats_cli.modules.structs.user import User
from pathlib import Path 
import json
import unittest

class TestAuth(unittest.TestCase):
    def setUp(self) :
        self.new_conversation = None
        self.message_handler = None
        self.conv_name = "albertpatrick"
        self.path_conv = Path(f"{os.environ['HISTORY_DIR']}/{self.conv_name}.json").resolve()
        self.user = User("Patrick")

        self.message_content = {
            "from" : "albert",
            "text" : "Message Handler test", 
            "file" : "", 
            "timestamp" : "1706024499.486117", 
            "isGroupChat" : [False]
        }

    def testCreateMessageHandler(self):
        self.message_handler = MessageHandler(self.user,None)
        self.assertTrue(type(self.message_handler) == MessageHandler)

    def testProcessChatMessage(self):
        self.message_handler = MessageHandler(self.user,self.message_content)
        # self.message_handler.process_received_chat_message(self.message_content)
      
        with open(Path(f"{os.environ['HISTORY_DIR']}/albertpatrick.json"),"r") as f:
            test_dict = json.load(f)
            found = False
            for d in test_dict["messages"]:
                if "timestamp" in d and d["timestamp"] == self.message_content["timestamp"]:
                    found = True
            
            self.assertTrue(found)

if __name__ == "__main__":
    if os.environ["HISTORY_DIR"] == "":
        raise Exception("You need to provide the path to the history/ dir for testing purposes")
    unittest.main()
