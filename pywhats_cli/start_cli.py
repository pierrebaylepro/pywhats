import threading
from pathlib import Path 
import getpass
from hashlib import sha256

import os
os.environ["HISTORY_DIR"] = str(Path(__file__).resolve().parent / "history")

from modules.py_client import PyWhatsClient
from modules.display_handler import DisplayHandler 
from modules.message_handler import MessageHandler
from modules.structs.user import User 

def erase_screen():
    os.system('cls' if os.name == 'nt' else 'clear')

def retrieve_login():
    login = ""
    password = ""

    while login == "":
        os.system('cls' if os.name == 'nt' else 'clear')
        login = input("login : ")
    while password == "":
        erase_screen()
        password = sha256(getpass.getpass(f"login : {login}\npassword : ").encode("utf-8")).hexdigest()

    return (login,password)




if __name__ == "__main__":
    IPaddress = "127.0.0.1"
    port = 45000

    display_handler = DisplayHandler()

    connection_success = False # Change this to be set while the authentication is done
    
    client_instance = PyWhatsClient(IPaddress, port,display_handler)
    
    client_instance.connect_to_server()

    while not connection_success:
        creds = retrieve_login()
        connection_success, err_message = client_instance.auth_user(creds)
        if err_message:
            print(err_message)
            

    if connection_success :
        client_instance.synchronize_conversations()
        erase_screen()
        print(display_handler.show_list_convs())
        sending_thread = threading.Thread(target= client_instance.input_handler)
        receiving_thread = threading.Thread(target = client_instance.receive_message)

        sending_thread.start()
        receiving_thread.start() 
    
    # while(1 == 1):
    #     pass
    #TODO ajouter la logique : on prend l'input du client jusqu'à ce que la commande soit bonne
    #ou alors ce n'était pas la commande et on envoit après avoir préparé le message