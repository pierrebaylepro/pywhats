import socket
import logging
import datetime
import threading
import json
import multiprocessing
import modules.struct.user as user 
import modules.error_handler as error_handler  
from datetime import datetime
from time import sleep
from hashlib import md5

from modules.message_handler import MessageHandler
from modules.misc.pywhats_exceptions import PyWhatsException  
from modules.conversation_handler import ConversationHandler


class ClientHandler(threading.Thread):

    def __init__(self, connection: socket.socket, addr,list_connected_users : list, lock : threading.Lock):
        super().__init__()
        self.connection = connection
        self.addr = addr
        self.endConnection = False

        # identity management
        self.user = None
        self.token = None
        self.list_connected_users = list_connected_users
        self.lock = lock

        # Timer allows use to not use an infinte loop and still run the method several times, with interval defined

        self.thread_message_transmitter = multiprocessing.Process(target=self.transmit_unread_chats)

    def run(self):
        logging.info(f"New client connected at {datetime.today().strftime('%Y-%m-%d %H:%M:%S')}")
        
        self.authentication_user()
        with self.lock:
            self.list_connected_users.append(self.user.get_username())
        
        # Identify the conversations for this client/user and 
        self.synchronize_conversation()

        # Start monitoring message to transmit to this user
        self.thread_message_transmitter.start()

        while not self.endConnection :
            self.readRequest()

        logging.debug("fin de la connection")
        self.endConnection = True
        with self.lock:
            self.list_connected_users.remove(self.user.get_username())
 
        self.thread_message_transmitter.terminate()

    def readRequest(self):
        
        payload = self.connection.recv(8096)
        message = payload.decode("utf-8")

        if message == "":
            self.endConnection = True

        try:
            # Transform the received message to a dict
            message_dict = json.loads(message)    

            if "exit" in message_dict["packet_type"] : # Change this by using a dict has payload that has a "command" key and another for text or else
                self.endConnection = True
    
            mh = MessageHandler(message_dict)
            mh.processMessage(self.user)

        except json.JSONDecodeError:
            logging.critical("Packet was not structured as a dict")
            error_handler.sendError(self.connection,self.addr,"WRONG_FORMAT")
            return 0
        
        except KeyError as e:
            logging.critical(f"Did not find expected {e.args[0]}")
            error_handler.sendError(self.connection,self.addr,"WRONG_FORMAT")
            return 0
        
        except PyWhatsException as e:
            error_handler.sendError(self.connection,self.addr,e.format)
  
        except ValueError as e:
            logging.critical("Packet did not fit the format")
            error_handler.sendError(self.connection,self.addr,"WRONG_FORMAT")

    def createAuthToken(self):
        if self.user == None:
            logging.critical("User has not been authentified")
            error_handler.sendError(self.connection,self.addr,"NOT_AUTHENTIFIED")

        self.token = "token"

        return "token"

    # Need to define how tokens are created and then refactore its check
    def checkAuthToken(self, token: str):
        if token == "token":
            return True
        else:
            logging.critical("Using an invalid token")
            error_handler.sendError(self.connection,self.addr,"NOT_AUTHENTIFIED")
            return False

    def authentication_user(self):
        while self.user is None:
            auth_message = self.connection.recv(8096).decode("utf-8")
            message_dict = json.loads(auth_message)

            try:
                mh = MessageHandler(message_dict)
                self.user = mh.process_auth_message()
            except PyWhatsException as e:
                self.user = None
                conn_err_message = mh.get_error_auth_message(e.format)
                self.connection.send(json.dumps(conn_err_message).encode("utf-8"))

        auth_message = json.dumps(mh.get_success_auth_message(self.createAuthToken()))
        self.connection.send(auth_message.encode("utf-8"))

        print("succes conn")

        # else:
        #     self.checkAuthToken(message_dict["token"])
        #     # Vérifier token
    
    # Iterate through the client's existing conversations to send new chats
        
    def transmit_unread_chats(self):
        while not self.endConnection:    
            # print(f"Checking for new messages for {self.user.get_username()}")
            # Check if a conversation has unread chats
            # if yes transmit the chats from the other user
            # from the last transmittion
            list_user_conv = ConversationHandler().get_user_conversation(self.user.get_username())
            for conversation in list_user_conv:
                if conversation.get_status().lower() == "unread" :
                    last_transmission = datetime.fromisoformat(conversation.get_last_transmission())
                    chats = conversation.get_chats(True)
                    for chat in chats:
                        chat_timestamp = datetime.fromisoformat(chat["timestamp"])
                        if chat["user"].lower() != self.user.get_username().lower() and chat_timestamp >= last_transmission:
                            print(f"Chat to send : {chat}")
                            chat_message = MessageHandler().get_chat_message(chat,conversation.get_users())
                            chat_message["token"] = self.token
                            
                            print(f"Chat MESSAGE to send : {chat_message}")
                            self.connection.send(json.dumps(chat_message).encode("utf-8"))
                            
                            conversation.set_last_transmission(datetime.isoformat(datetime.now(),sep="T",timespec="seconds"))
                            conversation.set_status("transmitted")
                            with self.lock:
                                conversation.save_conversation()
                        else:
                            break
            
    def synchronize_conversation(self):
        list_user_conv = ConversationHandler().get_user_conversation(self.user.get_username())
        
        for conversation in list_user_conv:
            conv_name = ""
            checksum = ""

            conv_name = conversation.get_users()
            conv_name.sort() 
            conv_name = ''.join(conv_name).lower()

            checksum = conversation.get_checksum()

            sync_message = MessageHandler().get_sync_message((conv_name,checksum))
            sync_message["token"] = self.token
            
            message = json.dumps(sync_message).encode("utf-8")
            # message_size = len(message).to_bytes(4, byteorder='big')
            # self.connection.send(message_size)
            self.connection.send(message)
            sync_resp = self.connection.recv(8096).decode("utf-8")
            sync_resp = json.loads(sync_resp)
            
            if not sync_resp["content"]["up_to_date"] :
                if sync_resp["content"]["timestamp"] is not None :
                    new_chats = conversation.get_chats_from(sync_resp["content"]["timestamp"])
                else :
                    new_chats = conversation.get_chats()

                for chat in new_chats:
                    message = MessageHandler().get_chat_message(chat,conversation.get_users())
                    encoded_message = json.dumps(message).encode("utf-8")
                    self.connection.send(encoded_message)

                self.connection.send(json.dumps("chat_sync_finished").encode("utf-8"))
                acquit = self.connection.recv(8096)

        sync_finished = MessageHandler().get_end_sync_message()
        sync_finished["token"] = self.token
        encoded_sync_finished = json.dumps(sync_finished).encode("utf-8")
        self.connection.send(encoded_sync_finished)