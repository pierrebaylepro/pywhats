import json
import logging
import os
from pathlib import Path
from modules.struct.conversation import Conversation

CONV_DIR = os.environ["CONV_DIR"]

class ConversationHandler:
    
    #################
    # Private methods

    def __init__(self, conv_name: str = None):
        self.conversation = None
        self.conv_path = ""

        self._load_conversation(conv_name)
        # logging.info(f"Loading {conv_name}")

    def _load_conversation(self, conv_name):
        if conv_name and self._conversation_file_exists(conv_name):
            self.conv_path = self._get_conversation_path(conv_name)
            self.conversation = Conversation(self.conv_path)
            self.conversation.load_conversation()

    def _conversation_file_exists(self, conv_name):
        return Path(self._get_conversation_path(conv_name)).exists()

    def _get_conversation_path(self, conv_name):
        return f"{CONV_DIR}/{conv_name}.json"

    def _initialize_new_conversation(self, users):
        self.conv_path = self._get_conversation_path(''.join(users).lower())
        self.conversation = Conversation(self.conv_path)
        self.conversation.set_users = users
        self.conversation.status = "new"
        self.conversation.save_conversation()
    
    
    #################
    # Public methods

    def create_conversation(self, users: list):
        users.sort()
        title = " et ".join(users)
        self._initialize_new_conversation(users)

    def get_conv_title(self):
        return self.conversation.get_title()
    
    def get_conv_status(self):
        return self.conversation.status
    
    def get_conv_chats(self, reverse : bool = False):
        return self.conversation.get_chats(reverse)
    
    def conv_exists(self):
        return self.conversation is not None

    def set_conv_status(self,status : str):
        self.conversation.status = status
    
    def get_user_conversation(self, username : str):
        # Since the username is a key of the file name we use it
        # to select the file where it appears then we check the users 
        # in the file (could be that the username is a part of another's username)
        
        list_conv = []

        for file in Path(CONV_DIR).iterdir():
            if username in file.name.strip(".json"):
                conv_obj = Conversation(str(file.absolute()))
                conv_obj.load_conversation()
                if username in conv_obj.get_users():
                    list_conv.append(conv_obj)

        return list_conv

    def add_chat(self,chat : dict, exp : str):
        
        new_chat = {
        "timestamp": None,
        "user": None,
        "content": {
          "text": None,
          "file": None
        }
      }
        
        new_chat["timestamp"] = chat.get("timestamp")
        new_chat["user"] = exp
        new_chat["content"]["text"] = chat.get("text","")
        new_chat["content"]["file"] = chat.get("file","")
        
        self.conversation.chats.append(new_chat)
        self.conversation.set_status("unread")
        self.conversation.save_conversation()
    
    def view_conversation(self):
        list_chats = self.conversation.get_chats()
        
        user_displayed = ""
        conversation_str = ""
        for chat in list_chats:
            if user_displayed != chat["user"]:
                user_displayed = chat["user"]
                conversation_str += f"\n{user_displayed.capitalize()}:\n"

            conversation_str += f"[{chat['timestamp'].split('T')[1]}] {chat['content']['text']}\n"    

        return conversation_str

    def new_chat():
        pass