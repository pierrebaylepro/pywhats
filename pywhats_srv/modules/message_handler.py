from modules.struct.user import User
from modules.conversation_handler import ConversationHandler

BASIC_MESSAGE = {
    "packet_type" : "",
    "content": None, 
    "identifier" : "",
    "token" : ""
}

class MessageHandler:

    def __init__(self, message: dict = None):
        self.message = message
        if message is not None:
            self.checkMessage()

    def checkMessage(self):
        for key in BASIC_MESSAGE.keys():
            if str(key) not in self.message.keys():
                raise KeyError
            
    def processMessage(self, user : User = None):
        if self.message["packet_type"] == "chat":
            print(f"Received this chat : {self.message['content']}")
            self.process_chat_message(self.message['content'], user)
        if self.message["packet_type"] == "connect":
            print(f"Received authentication message for {self.message['content']}")
            return self.process_auth_message()

    def process_chat_message(self, chat_content, user: User):
        conv_name = list(chat_content["to"]) 
        conv_name.append(user.get_username())
        conv_name.sort()
        conv_name = ''.join(conv_name)

        conv_handler = ConversationHandler(conv_name)
        conv_handler.add_chat(chat_content,exp = user.get_username())


    def process_auth_message(self):
        if self.message["packet_type"] != "connect":
            return {}
            raise PyWhatsException("NOT_AUTHENTIFIED")
            
        connection_info = self.message["content"]
        return User(username=connection_info["username"], password=connection_info["password"])

    def get_success_auth_message(self,token):
        success_auth_message = {
        "packet_type": "connect",
        "content": {"token": "token"},
        "identifier": "",
        "token": token
        }

        return success_auth_message

    def get_error_auth_message(self,error : str) :
        error_auth_message = {
        "packet_type": "connect",
        "content": {"error" : error},
        "identifier": "",
        "token": "token"
        }

        return error_auth_message
    
    def get_chat_message(self, chat :dict,  groupChat : dict = None):
        chat_message = {
            "packet_type" : "chat",
            "content": {
                "from" : "",
                "file" : "",
                "text" : "",
                "timestamp" : "",
                "isGroupChat" : ""
            }, 
            "identifier" : "",
            "token" : ""
        }

        # Can be refactored once tested
        chat_message["content"]["from"] = chat["user"].lower()
        chat_message["content"]["file"] = chat["content"]["file"]
        chat_message["content"]["text"] = chat["content"]["text"]
        chat_message["content"]["timestamp"] = chat["timestamp"]
        
        if len(groupChat) > 2 :
            chat_message["content"]["isGroupChat"] = groupChat

        return chat_message
        
    def get_sync_message(self, conv_infos : tuple):
        sync_message = {
            "packet_type" : "sync",
            "content": {
                "conv_name" : "",
                "checksum" : "",
            }, 
            "identifier" : "",
            "token" : ""
        }

        sync_message["content"]["conv_name"] = conv_infos[0]
        sync_message["content"]["checksum"] = conv_infos[1]
        
        return sync_message
        
    def get_end_sync_message(self):
        end_sync = {
            "packet_type" : "sync",
            "content": {
                "status" : "finished"
            }, 
            "identifier" : "",
            "token" : ""
        }

        return end_sync