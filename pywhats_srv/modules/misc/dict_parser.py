import json

def parse_string_for_dict(string_to_parse : str):
    parsed_dicts = []

    stack = []
    start_index = 0

    # Iterate over the string
    for i in range(len(string_to_parse)):
        if string_to_parse[i] == '{':
            if not stack:
                start_index = i
            stack.append(i)
        # Check if the current character is the end of a dictionary
        elif string_to_parse[i] == '}':
            # Pop the last opening brace index from the stack
            stack.pop()
            # If the stack is empty, this is the end of the top-level dictionary
            if not stack:
                dict_str = string_to_parse[start_index:i+1]
                parsed_dict = json.loads(dict_str)
                parsed_dicts.append(parsed_dict)

