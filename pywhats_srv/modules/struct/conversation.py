import json
import logging 
from datetime import datetime
from hashlib import sha256

#           Exemple 
#    {
#     "users": ["Alice","Bob"],
#     "last_transmission" : "2023-01-21T12:00:00"
#     "state": "unread",
#     "chats": [
#       {
#         "timestamp": "2023-01-21T12:00:00",
#         "user": "Bob",
#         "content": {
#           "texte": "Test message 1",
#           "file": ""
#         }
#       }
#     ]
#     }

class Conversation:
    
    def __init__(self,file_path : str):
        self.file = file_path
    
        self.status = ""
        self.users = ""
        self.chats = []
        self.last_transmission = ""

    
    def load_conversation(self):
        try:
            with open(self.file, 'r', encoding='utf-8') as file:
                dict_from_conv = json.load(file)

                self.users = dict_from_conv.get("users",[])
                self.status = dict_from_conv.get("status", "")
                self.chats = dict_from_conv.get("chats", [])
                self.last_transmission = dict_from_conv.get("last_transmission","")

        except FileNotFoundError:
            print(f"Le fichier {self.file} n'a pas été trouvé.")
            return None
        except json.JSONDecodeError:
            print(f"Erreur lors du décodage du fichier {self.file}. Le fichier JSON est mal formé.")
            return None
        except Exception as e:
            print(f"Une erreur inattendue s'est produite : {e}")
            return None
        
    def save_conversation(self):
        try:
            with open(self.file, "w", encoding="utf-8") as file:
                data = {
                    "users": self.users,
                    "status": self.status,
                    "last_transmission": self.last_transmission,
                    "chats": self.chats
                }
                json.dump(data, file, ensure_ascii=False, indent=4)
            logging.info(f"Conversation saved successfully to {self.file}")
        except Exception as e:
            logging.critical(f"Error saving conversation to {self.file}: {e}")

    def get_title(self,users : list = None):
        if users is None:
            users = self.users

        title = ""
        for i in range(len(users)):
            if i + 1 == len(users) and i != 0:
                title += " et "
            elif i != 0:
                title += ", "
            
            title += users[i].capitalize()
        
        return title
    
    def get_users(self):
        return self.users
    
    def get_status(self):
        return self.status
    
    def get_chats(self, reverse : bool = False):
            if len(self.chats) != 0:    
                return sorted(self.chats, key=lambda d: d['timestamp'], reverse=reverse)
            else:
                Exception("No chats")
    
    # Get all the chats from the conversation ordered in reverse chronological order
    # if a chat timestamp is after the timestamp to search from append it to the result list
    # else stop searching
                
    def get_chats_from(self, timestamp : str):
        timestamp_from = datetime.fromisoformat(timestamp)
        all_chats = self.get_chats(True)

        chats_from = []
        
        for chat in all_chats:
            timestamp_chat = datetime.fromisoformat(chat["timestamp"])
            if timestamp_chat > timestamp_from:
                chats_from.append(chat)
            else :
                break
        
        return chats_from
    
    def get_last_transmission(self):
        return self.last_transmission
    
    def get_checksum(self):
        sha256_checksum = sha256(str(self.get_chats()).encode("utf-8")) 
        return sha256_checksum.hexdigest()
    
    def set_status(self,status : str):
        self.status = status

    def set_last_transmission(self,timestamp : str):
        self.last_transmission = timestamp
    
    def set_users(self,users : dict):
            self.users = users