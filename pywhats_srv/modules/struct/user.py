import json
import logging
from modules.misc.pywhats_exceptions import PyWhatsException  

import os
USER_DB = os.environ["USER_DB"]

class User:

    def __init__(self, username: str, password: str):
        self.username = username
        self.userInfos = None

        try:
            with open(USER_DB,"r") as users:
                dict_users = json.load(users)
                if username not in dict_users.keys():
                    raise PyWhatsException("WRONG_USER")
                current_user = dict_users[username]
                if current_user["password"] != password:
                    raise PyWhatsException("WRONG_PASSWORD")


                self.userInfos = dict_users[f"{username}"]
                
        except OSError:
            logging.critical("Could not load users database")
            raise PyWhatsException("SERVER_MALFUNCTION")
        
    def get_username(self):
        return self.username
    

    