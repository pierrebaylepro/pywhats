import os
os.environ["USER_DB"] = "/media/eff/4A4241B74241A891/Users/Admin/Desktop/M1/projetPython/pywhats/pywhats_srv/user_base.json"

from user_handler import UserHandler
from misc.pywhats_exceptions import PyWhatsException  
import unittest
import json
import random
import string
from hashlib import sha256

USER_DB = os.environ["USER_DB"]

class TestUser(unittest.TestCase):
    def setUp(self) :
        self.uh = UserHandler()
        
    def testCreateUser(self):
        letters = string.ascii_lowercase
        username = ''.join(random.choice(letters) for i in range(10))
        password = sha256(b"test_password").hexdigest()
        self.uh.create_user(username,password)
        
        print(f"test user = {username}\npassword = {password}\n")

        with open(USER_DB,"r") as user_db:
            dict_db = json.load(user_db)
            self.assertTrue(username in dict_db.keys())          
        with open(USER_DB,"w") as user_db:
            dict_db.pop(username)
            json.dump(dict_db,user_db,indent=4)
            
if __name__ == "__main__":
    unittest.main()