import logging 
import os
from pathlib import Path 
import json
from modules.struct.user import User 

USER_DB = os.environ["USER_DB"]

class UserHandler():
    
    def __init__(self, user : User = None) :
        self.user = user
    
    def _user_exists_(slef,username : str) :
        with open(USER_DB,"r") as user_db:
            dict_db = json.load(user_db)
            user_db.close()
            return username in dict_db.keys()
        
    # Receives a username and a hash of the password 
    def create_user(self,username : str, password : str):
        if self._user_exists_(username):
            return False
        
        with open(USER_DB,"r") as user_db:
            user_db_dict = json.load(user_db)
            user_db.close()
        with open(USER_DB,"w") as user_db:
            user_db_dict.update({username : {"password" : password}})
            json.dump(user_db_dict,user_db,indent=4)
            user_db.close()
        
        self.user = User(username,password)
        return True