from pathlib import Path
import os
os.environ["USER_DB"] = str(Path(__file__).resolve().parent / "user_base.json")
os.environ["CONV_DIR"] = str(Path(__file__).resolve().parent / "conversations")

import json
import conf.config as config
import modules.py_serv as py_serv
import logging

conf = config.Config()
PATH_USER_DB = str(Path(__file__).resolve().parent / "conf/parameters.json")

try:

    with open(PATH_USER_DB,"r") as conf_file:
        dict_conf = json.load(conf_file)
        conf.loadConfig(dict_conf)

except ValueError as e:
    logging.critical(e)
    print("Server shutting down")
    exit(0)

if __name__== "__main__" :
    print("Hello world")
    PyWhatSrv = py_serv.PyWhatsServer(conf.SERVER_PORT)
    PyWhatSrv.startServer()    

    